<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/products');
});

Route::get('/login', [AuthController::class, 'index']);
Route::post('/login', [AuthController::class, 'login']);


Route::get('/products', [ProductController::class, 'index']);
Route::get('/product/create', [ProductController::class, 'create']);
Route::post('/product/create/submit', [ProductController::class, 'createSubmit']);
Route::get('/product/{id}/edit', [ProductController::class, 'edit']);
Route::put('/product/{id}/edit/submit', [ProductController::class, 'editSubmit']);
Route::get('/product/{id}/delete', [ProductController::class, 'delete']);
Route::delete('/product/{id}/delete/submit', [ProductController::class, 'deleteSubmit']);

Route::get('/users', [UserController::class, 'index']);
Route::get('/user/create', [UserController::class, 'create']);
Route::post('/user/create/submit', [UserController::class, 'createSubmit']);
Route::get('/user/{id}/edit', [UserController::class, 'edit']);
Route::post('/user/{id}/edit/submit', [UserController::class, 'editSubmit']);
Route::get('/user/{id}/delete', [UserController::class, 'delete']);
Route::delete('/user/{id}/delete/submit', [UserController::class, 'deleteSubmit']);