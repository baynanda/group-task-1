<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    public function index(){
        $i = User::all();
        return view('user.list', compact(['i']));
    }
    public function create(){
        return view('user.create');
    }
    public function createSubmit(Request $r){
        if($r->passwordProposed == $r->passwordRetype){
            $r->merge(['password' => $r->passwordProposed]);
            User::create($r->except(['_token', 'passwordProposed', 'passwordRetype']));
            return redirect('/users');
        }
        else{
            return back()
            ->with('error','Password is not match');
        }
    }
    public function edit($id){
        $i = User::find($id);
        return view('user.edit', compact(['i']));
    }
    public function editSubmit(Request $r, $id){
        $i = User::find($id);
        $i->update($r->except(['_token', 'passwordProposed', 'passwordRetype']));
        return redirect('/users');
    }
    public function delete($id){
        $i = User::find($id);
        return view('user.delete', compact(['i']));
    }
    public function deleteSubmit($id){
        User::find($id)->delete();
        return redirect('/users');
    }
}
