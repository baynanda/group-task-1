@extends('layout.main')

@section('content')
<div class="row container-fluid justify-content-center">
    <div class="col-8 pb-3">
        <h3>Delete Product "{{$i->name}}"?</h3>
    </div>
    <div class="col-8">
        <div class="py-1 row">
            <div class="col">
                <label class="form-label" for="inputName">Name</label>
                <input type="text" name="name" id="inputName" class="form-control" value="{{$i->name}}" disabled>
            </div>
            <div class="col">
                <label class="form-label" for="inputDescription">Description</label>
                <input type="text" name="description" id="inputDescription" class="form-control"
                    value="{{$i->description}}" disabled>
            </div>
        </div>
        <div class="py-1 row">
            <div class="col">
                <label class="form-label" for="inputBuy">Buy</label>
                <input type="text" name="buy" id="inputBuy" class="form-control" value="{{$i->buy}}" disabled>
            </div>
            <div class="col">
                <label class="form-label" for="inputSell">Sell</label>
                <input type="text" name="sell" id="inputSell" class="form-control" value="{{$i->sell}}" disabled>
            </div>
        </div>
        <div class="py-1">
            <label class="form-label" for="inputImagePath">Image Path</label>
            <input type="text" name="iamge_path" id="inputImagePath" class="form-control" value="{{$i->image_path}}"
                disabled>
        </div>
        <div class="row justify-content-start py-2">
            <form action="/product/{{$i->id}}/delete/submit" method="POST">
                @csrf
                @method('delete')
                <button type="submit" class="btn btn-danger">Confirm Delete <i class="fa fa-trash"
                        aria-hidden="true"></i></button>
            </form>
        </div>
    </div>
</div>
@endsection
