@extends('layout.main')

@section('content')
<div class="row container-fluid justify-content-center">
    <div class="col-8 pb-3">
        <h3>Create Product</h3>
    </div>
    <div class="col-8">
        <form action="/product/create/submit" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="py-1 row">
                <div class="col">
                    <label class="form-label" for="inputName">Name</label>
                    <input type="text" name="name" id="inputName" class="form-control" placeholder="product name">
                </div>
                <div class="col">
                    <label class="form-label" for="inputDescription">Description</label>
                    <input type="text" name="description" id="inputDescription" class="form-control"
                        placeholder="product description">
                </div>
            </div>
            <div class="py-1 row">
                <div class="col">
                    <label class="form-label" for="inputBuy">Buy</label>
                    <input type="text" name="buy" id="inputBuy" class="form-control" placeholder="buy value">
                </div>
                <div class="col">
                    <label class="form-label" for="inputSell">Sell</label>
                    <input type="text" name="sell" id="inputSell" class="form-control" placeholder="sell value">
                </div>
            </div>
            <div class="py-1">
                <label class="form-label" for="inputImage">Image (jpg, jpeg, png) - optional*</label>
                <input type="file" name="raw_image" id="inputImage"
                    class="form-control @error('file') is-invalid @enderror">
            </div>
            <div class="row justify-content-start py-2">
                <div class="col-2"><button type="submit" class="btn btn-success">Submit <i class="fa fa-check"
                            aria-hidden="true"></i></button></div>
                <div class="col-2"><button type="reset" class="btn btn-danger">Reset <i class="fa fa-times"
                            aria-hidden="true"></i></button></div>
            </div>
            <div class="container">
                @error('(jpg, jpeg, png)')
                <span class="text-danger">{{ $message }}</span><br>
                @enderror
            </div>
        </form>
    </div>
</div>
@endsection
