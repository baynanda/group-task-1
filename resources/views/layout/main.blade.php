<html>

<head>
    <title>Purrr..niture</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous">
    </script>

</head>

<body style="background-color: #f0f0f0;">
    <nav class="navbar navbar-expand-lg shadow-sm" style="background-color: #009bff;">
        <div class="container-fluid">
            <a class="navbar-brand text-light col-2 fw-bold" href="/products">/ᐠ｡ꞈ｡ᐟ\ Purrr..niture</a>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a href="/products" class="btn btn-dark mx-2"><i class="fa fa-database" aria-hidden="true"></i>
                            Products</a>
                    </li>
                    <li class="nav-item">
                        <a href="/users" class="btn btn-dark mx-2"><i class="fa fa-users" aria-hidden="true"></i>
                            User</a>
                    </li>
                </ul>
            </div>
            <div class="d-flex">
                <h6 class="my-auto me-3 text-light">Hello, admin</h6>
                <ul class="navbar-nav me-auto">
                    <li class="nav-item">
                        <button type="button" class="btn btn-danger mx-2">Logout <i class="fa fa-sign-out"
                                aria-hidden="true"></i></button>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <main class="py-4">
        @yield('content')
    </main>
</body>

</html>
