@extends('layout.main')

@section('content')
<div class="row container-fluid justify-content-center">
    <div class="col-8 pb-3">
        <h3>Delete User "{{$i->name}}"?</h3>
    </div>
    <div class="col-8">
        <div class="py-1 row">
            <div class="col">
                <label class="form-label" for="inputName">Username</label>
                <input type="text" name="name" id="inputName" class="form-control" value="{{$i->name}}" disabled>
            </div>
            <div class="col">
                <label class="form-label" for="inputDescription">Description</label>
                <input type="text" name="description" id="inputDescription" class="form-control"
                    value="{{$i->description}}" disabled>
            </div>
        </div>
        <div class="py-1 row">
            <div class="col">
                <label class="form-label" for="inputBirthPlace">Birth Place</label>
                <input type="text" name="birthPlace" id="inputBirthDate" class="form-control" value="{{$i->birthPlace}}"
                    disabled>
            </div>
            <div class="col">
                <label class="form-label" for="inputBirthDate">Birth Date</label>
                <input type="date" name="birthDate" class="form-control" id="inputBirthDate" value="{{$i->birthDate}}"
                    disabled>
            </div>
        </div>
        <div class="py-1 row">
            <div class="col">
                <label class="form-label" for="inputEmail">Email</label>
                <input type="text" name="email" id="inputEmail" class="form-control" value="{{$i->email}}" disabled>
            </div>
            <div class="col">
                <label class="form-label" for="inputGender">Gender</label><br>
                <div class="btn-group" role="group">
                    <input type="radio" name="gender" class="btn-check" id="btncheck1" autocomplete="off" value="M"
                        @if($i->gender == "M") checked @endif disabled>
                    <label class="btn btn-outline-primary" for="btncheck1">Male</label>

                    <input type="radio" name="gender" class="btn-check" id="btncheck2" autocomplete="off" value="F"
                        @if($i->gender == "F") checked @endif disabled>
                    <label class="btn btn-outline-primary" for="btncheck2">Female</label>
                </div>
            </div>
        </div>
        <div class="row justify-content-start py-2">
            <form action="/user/{{$i->id}}/delete/submit" method="POST">
                @csrf
                @method('delete')
                <button type="submit" class="btn btn-danger">Confirm Delete <i class="fa fa-trash"
                        aria-hidden="true"></i></button>
            </form>
        </div>
        @if ($message = Session::get('error'))
        <div class="alert alert-danger alert-block">
            <strong>{{ $message }}</strong>
        </div>
        @endif
    </div>
</div>
@endsection
