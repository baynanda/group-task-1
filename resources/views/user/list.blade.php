@extends('layout.main')

@section('content')
<div class="row container-fluid justify-content-center">
    <div class="col-8 pb-3">
        <a href="/user/create" class="btn btn-success">Create <i class="fa fa-plus-circle" aria-hidden="true"></i></a>
    </div>
    <div class="col-8">
        <table class="table table-striped table-hover table-bordered">
            <tr>
                <th>No</th>
                <th>Name</th>
                <th>Description</th>
                <th>Birth Place</th>
                <th>Birth Date</th>
                <th>Email</th>
                <th>Action</th>
            </tr>
            <?php $a=1;?>
            @foreach($i as $i)
            <tr>
                <td>{{$a}}</td>
                <td>{{$i->name}}</td>
                <td>{{$i->description}}</td>
                <td>{{$i->birthPlace}}</td>
                <td>{{$i->birthDate}}</td>
                <td>{{$i->email}}</td>
                <td>
                    <a href="/user/{{$i->id}}/edit" class="btn btn-warning text-white"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                    <a href="/user/{{$i->id}}/delete" class="btn btn-danger text-white"><i class="fa fa-trash" aria-hidden="true"></i></a>
                </td>
            </tr>
            <?php $a++;?>
            @endforeach
        </table>
    </div>
</div>
@endsection
