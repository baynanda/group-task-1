@extends('layout.main')

@section('content')
<div class="row container-fluid justify-content-center">
    <div class="col-8 pb-3">
        <h3>Create User</h3>
    </div>
    <div class="col-8">
        <form action="/user/create/submit" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="py-1 row">
                <div class="col">
                    <label class="form-label" for="inputName">Username</label>
                    <input type="text" name="name" id="inputName" class="form-control" placeholder="user name">
                </div>
                <div class="col">
                    <label class="form-label" for="inputDescription">Description</label>
                    <input type="text" name="description" id="inputDescription" class="form-control"
                        placeholder="user description">
                </div>
            </div>
            <div class="py-1 row">
                <div class="col">
                    <label class="form-label" for="inputBirthPlace">Birth Place</label>
                    <input type="text" name="birthPlace" id="inputBirthDate" class="form-control"
                        placeholder="birth place">
                </div>
                <div class="col">
                    <label class="form-label" for="inputBirthDate">Birth Date</label>
                    <input type="date" name="birthDate" class="form-control" id="inputBirthDate">
                </div>
            </div>
            <div class="py-1 row">
                <div class="col">
                    <label class="form-label" for="inputEmail">Email</label>
                    <input type="text" name="email" id="inputEmail" class="form-control" placeholder="user@email.com">
                </div>
                <div class="col">
                    <label class="form-label" for="inputGender">Gender</label><br>
                    <div class="btn-group" role="group">
                        <input type="radio" name="gender" class="btn-check" id="btncheck1" autocomplete="off" value="M">
                        <label class="btn btn-outline-primary" for="btncheck1">Male</label>

                        <input type="radio" name="gender" class="btn-check" id="btncheck2" autocomplete="off" value="F">
                        <label class="btn btn-outline-primary" for="btncheck2">Female</label>
                    </div>
                </div>
            </div>
            <div class="py-1 row">
                <div class="col">
                    <label class="form-label" for="inputPassword">Password</label>
                    <input type="password" name="passwordProposed" id="inputPassword" class="form-control"
                        placeholder="password">
                </div>
                <div class="col">
                    <label class="form-label" for="inputPasswordRetype">Re-type Password</label>
                    <input type="password" name="passwordRetype" id="inputPasswordRetype" class="form-control"
                        placeholder="retype password">
                </div>
            </div>
            <div class="row justify-content-start py-2">
                <div class="col-2"><button type="submit" class="btn btn-success">Submit <i class="fa fa-check"
                            aria-hidden="true"></i></button></div>
                <div class="col-2"><button type="reset" class="btn btn-danger">Reset <i class="fa fa-times"
                            aria-hidden="true"></i></button></div>
            </div>
        </form>
        @if ($message = Session::get('error'))
        <div class="alert alert-danger alert-block">
            <strong>{{ $message }}</strong>
        </div>
        @endif
    </div>
</div>
@endsection
