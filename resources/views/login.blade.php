<html>

<head>
    <title>Purrr..niture</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous">
    </script>
</head>

<body style="background-color: #009bff;">
    <div class="row d-flex justify-content-center align-items-center pt-5">
        <div class="col-3 mt-5 align-items-center p-3 rounded" style="background-color: #f0f0f0;">
            <h1 class="text-center" style="color:#3c3c3c">/ᐠ｡ꞈ｡ᐟ\</h1>
            <form action="/login" method="POST">
                @csrf
                <div class="pt-4 input-group">
                    <span class="input-group-text" id="basic-addon1"><i class="fa fa-user" aria-hidden="true"></i>
                    </span>
                    <input type="text" class="form-control" placeholder="Username" name="username" aria-label="Username"
                        aria-describedby="basic-addon1">
                </div>
                <div class="py-1 input-group">
                    <span class="input-group-text" id="basic-addon2"><i class="fa fa-lock" aria-hidden="true"></i>
                    </span>
                    <input type="text" class="form-control" placeholder="Password" name="password" aria-label="Password"
                        aria-describedby="basic-addon2">
                </div>
                <div class="d-flex justify-content-end py-2">
                    <div class=""><button type="submit" class="btn btn-success">Login <i class="fa fa-paper-plane"
                                aria-hidden="true"></i></button></div>
                </div>
            </form>
        </div>
    </div>
</body>

</html>
